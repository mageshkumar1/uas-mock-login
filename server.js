require( "dotenv" ).config();
const fastify = require( "fastify" )( { logger: true } );
fastify.register(require('fastify-jwt'), {
  secret: process.env.JWT_SECRET
});
fastify.register(require('fastify-formbody'));
fastify.register(require('fastify-cookie'), {
  secret: process.env.JWT_SECRET, // for cookies signature
  parseOptions: {}     // options for parsing cookies
})
const fs = require( "fs" );
const util = require( "util" );

// Declare a route
fastify.get('/', (request, reply) => {
  reply.send({ hello: 'world' })
})

// Run the server!

const readFile = util.promisify( fs.readFile );

fastify.route( {
  method: "POST",
  url: "/authx/uas/v1/introspect",
  preHandler: async ( request, reply ) => {
    
  },
  handler: async ( request, reply ) => {
    const obj = JSON.parse( await readFile( "sample-data.json", "utf8" ) );
	const userId = request.params.userId;
    return obj.token;
  }
} );
fastify.route( {
  method: "GET",
  url: "/v1/zone/schemas/:identifier/",
  schema: {
    querystring: {
      identifier: { type: "string" }
    }
  },
  
  preHandler: async ( request, reply ) => {
    
  },
  handler: async ( request, reply ) => {
    const obj = JSON.parse( await readFile( "sample-data.json", "utf8" ) );
	var accountInfo = obj.Accounts.find( r => (r.accessid === request.params.identifier || r.domainname === request.params.identifier) );

    if ( !accountInfo )
      return reply.code( 404 ).send();

    return accountInfo;
  }
} );
fastify.route( {
  method: "GET",
  url: "/authx/uas/portal/auth/login",
  preHandler: async ( request, reply ) => {
    
  },
  handler: async ( request, reply ) => {
    const token = fastify.jwt.sign({ userId: '1' });
	reply
    .setCookie('access_token', token, {
      domain: "poi-overview",
      path: '/',
	  secure: true,
      httpOnly: true
    });
    // save user token
    console.log("token"+token);
	reply
    .setCookie('foo', 'foo', {
      domain: 'example.com',
      path: '/'
    })
    .cookie('baz', 'baz') // alias for setCookie
    .setCookie('access_token', token, {
      path: '/',
      signed: true
    })
    .send(token);
    
  }
} );
const start = async () => {
  try {
    await fastify.listen( process.env.PORT,"poi-overview");
    fastify.log.info( `server listening on ${ fastify.server.address().port }` );
  } catch ( err ) {
    fastify.log.error( err );
    process.exit( 1 );
  }
};

start();