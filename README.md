# Mock Login 

This project used for mock login for developers. 

# Setup Guide

1) Clone the project.

2) run `npm install`

3) Please open the sample-data.json file, update the tenantid and user id as your wish and save the file.

4) Please use below command to run the server.
      **`node server`**

# nginx setup

Please add below entry in nginx.conf file

location /authx/ {
		    proxy_set_header Host $host;
		    proxy_pass http://192.168.3.59:7000;
			proxy_read_timeout 600s;
        }

# cougar setup

Please update the below key in app_properties table.

update app_properties set propertyvalue='http://{{hostname}}/authx' where propertykey='uas.server.url'

Please restart the cougar.

# generate accesstoken

Please use below api to get the accesstoken. Please copy the below api and run in your browser.

http://poi-overview/authx/uas/portal/auth/login .

You can see the token in browser cookie for the same domain.

Once accesstoken generated, you access the local site by calling http://poi-overview/home.do


# rialtini mock response


Please use below api to get the accountinfo response similar to rialtini service.

http://{ipaddress or hostname}:7000/v1/zone/schemas/{accessid or domainname}/

# adrem setup 

Please change the below properties in adrem 

oauth2 {
	uas-server = "http://poi-overview/authx/"
	uas-server = ${?OAUTH2_HYDRA_SERVER}
}

rialtini {
  base-url: "http://192.168.3.59:7000/"
  base-url: ${?RIALTINI_SERVICE_URL}
}

# pengo  or other micro services(boot2 based services) setup

Please change the below properties in pengo .

oauth:
  introspect:
    url: http://poi-overview/authx/uas/v1/introspect
  mobile:
    introspect:
      url: http://poi-overview/authx/uas/v1/introspect

rialtini:
  server: http://192.168.3.59:7000


**`Note:http://poi-overview or http://192.168.3.59 is my host and ipaddress. Please change this in your system based on your ip address or hostname`**
